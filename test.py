import SCM as scm

print("SCM Challenge")

print("\n\nNOTE: it is not necessary to include the error term. The library do it automatically!!\n\n")


print("Graph 1: one cyclic SCM")
print("=======================")
print("a := x + epsilon_a")
print("x := a + epsilon_x")
print("y := a + x + epsilon_y")

a = ['a', ['x'], 'x']
x = ['x', ['a'], 'a']
y = ['y', ['a', 'x'], 'a + x']

g1 = scm.SCM([a, x, y])

print("\n")
print("Graph 2: one correct SCM")
print("========================")
print("a := epsilon_a")
print("x := a + epsilon_x")
print("y := a + x + epsilon_y")

a = ['a', [], '1']
x = ['x', ['a'], 'a']
y = ['y', ['a', 'x'], 'a + x']

g2 = scm.SCM([a, x, y])

print("\nIs it a DAG?", g2.is_no_cycled())
print("\n5 samples for this SCM:")
print(g2.sample(5, seed=42))

print("\n")
print("Graph 3: Graph 2 in a different order")
print("=====================================")
g3 = scm.SCM([y, a, x])

print("\nIs it a DAG?", g2.is_no_cycled())
print("\n5 samples for this SCM:")
print(g3.sample(5, seed=42))

print("\nThe same result that before, but the columns are inverted!!")


