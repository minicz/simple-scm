import numpy as np
import pandas as pd
import random
import inspect

class SCM:
    """
    SCM is a small class to work with Structural Causal Models.
    
    Example:
    ========
        a = ['a', [], '1']
        x = ['x', ['a'], 'a']
        y = ['y', ['a', 'x'], 'a + x']

       g = scm.SCM([a, x, y])
       g.is_no_cycled()
       g.sample(20, seed=42)
    
    It is a experimental class and it doesn't check many things!!
    """
    def __init__(self, sem):
        self.sem = sem
        self.n_eq = len(sem)
        
        self.nodes = []
        for sem in self.sem:
            node = sem[0]
            self.nodes.append(node)
        
        if self.is_no_cycled() == False:
            print("The SCM is cyclic!!")
    
    def adjacency_matrix(self):
        """
        Construct a adjacency matrix. A simple way to make calculations about de graph!
        """
        adjacency_matrix = np.zeros(shape=(self.n_eq, self.n_eq))
        
        for sem in self.sem:
            for s in sem[1]:
                adjacency_matrix[self.nodes.index(s), self.nodes.index(sem[0])] = 1

        adjacency_matrix = adjacency_matrix.astype(int)
        
        return adjacency_matrix
    
    def is_no_cycled(self):
        """
        Checks if the SEM is a directed acyclic graph. Uses the characterisation of
        acyclicity established by D. Wei, T. Gao and Y. Yu in [1].

        .. [1] Wei, D., Gao, T. and Yu, Y. "DAGs with No Fears : A Closer Look
           at Continuous Optimization for Learning Bayesian
           Networks". *Advances in Neural Information Processing Systems*,
           volume 33, pp. 3895-3906, 2020.
        """
        adjacency_matrix = self.adjacency_matrix()
 
        eigenvalues = np.linalg.eigvals(adjacency_matrix)
        comparand = np.zeros_like(eigenvalues)

        return np.allclose(eigenvalues, comparand, atol=1e-6)
    
    def sample(self, n_samples=5, seed=None):
        """
        The return is a Pandas DataFrame.
        
        You can set the seed and the number of samples.
        
        Before samples it is necessary to know the correct order to obtem the values.
        It is made using the well know Kahn's algorithm.
        """
        if (seed != None):
            random.seed(seed)
        
        data = pd.DataFrame(
            np.empty((n_samples, self.n_eq,)),
            index=range(n_samples),
            columns=self.nodes
        )
        
        #
        # Start Kahn's algorithm
        adjacency = self.adjacency_matrix()
        
        indegrees = []
        for sem in self.sem:
            indegrees.append(len(sem[1]))
        
        queue = np.where(np.asarray(indegrees) == 0)[0].tolist()
        sem_ordered = []
        
        while queue:
            current = queue.pop(0)
            sem_ordered.append(current)
            
            for neighbour in [n for n in range(len(adjacency[current])) if adjacency[current][n] == 1]:
                indegrees[neighbour] -= 1
                if indegrees[neighbour] == 0:
                    queue.append(neighbour)
        # End the Kahn's algorithm
        #
        
        for sample in range(n_samples):
            dct =  {i: 0 for i in self.nodes}
            for s in sem_ordered:
                dct[self.sem[s][0]] = eval(self.sem[s][2], dct) + random.random()
            data.loc[sample,:] = dct
        
        return data