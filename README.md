# Simple SCM

This simple SCM class is my participation on SCM Challenge from [Causal Python](https://causalpython.io).¶

The rules are following:

- Include noise terms (any distributions you want)
- No cycles (variables cannot cause themselves neither directly nor indirectly)
- Implementation should be object-oriented
- Your SCM should have a .sample() method that takes sample size and returns a sample from the model (all variables or just the outcome variable - your choice)
